package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class WhiteThread extends ColorThread {

    private static final String MESSAGE = "hi back blacks, hi back blues";

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    CountDownLatch latch;
    public WhiteThread(CountDownLatch latch) {
        this.latch = latch;
    }
    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        printMessage();
        this.latch.countDown();
    }
}
