package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PI
{
    private static BigDecimal pi;

    synchronized public static void endComputation(BigDecimal bd) {
        pi = pi.add(bd);
    }

    public static BigDecimal getPi() {
        return pi;
    }

    public static void resetPi() {
        pi = new BigDecimal(0).setScale(1000, RoundingMode.HALF_DOWN);
    }

}
