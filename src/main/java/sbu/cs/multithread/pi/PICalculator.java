package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint)
    {
        CountDownLatch latch = new CountDownLatch(1000);
        ExecutorService executor = Executors.newFixedThreadPool(8);

        PI.resetPi();

        for (int i = 0; i < 1000; i++) {
            executor.submit( new Calculator(i, latch) );
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();

        BigDecimal ans = PI.getPi();
        return ans.setScale(floatingPoint, RoundingMode.DOWN).toString();
    }
}
