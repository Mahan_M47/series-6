package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.CountDownLatch;

public class Calculator implements Runnable
{
    private final int n;
    private final MathContext mc = new MathContext(1100);
    private final CountDownLatch latch;

    public Calculator (int counter, CountDownLatch latch) {
        this.n = counter;
        this.latch = latch;
    }

    @Override
    public void run()
    {
        final BigDecimal ONE  = new BigDecimal(1);
        final BigDecimal TWO  = new BigDecimal(2);
        final BigDecimal FOUR = new BigDecimal(4);

        BigDecimal SIXTEEN = new BigDecimal(16);
        SIXTEEN = SIXTEEN.pow(n, mc);
        SIXTEEN = ONE.divide(SIXTEEN, mc);

        BigDecimal A = new BigDecimal(8*n + 1);
        A = FOUR.divide(A, mc);

        BigDecimal B = new BigDecimal(8*n + 4);
        B = TWO.divide(B, mc);

        BigDecimal C = new BigDecimal(8*n + 5);
        C = ONE.divide(C, mc);

        BigDecimal D = new BigDecimal(8*n + 6);
        D = ONE.divide(D, mc);

        BigDecimal ans = A;
        ans = ans.subtract(B).subtract(C).subtract(D);

        ans = ans.multiply(SIXTEEN, mc).setScale(1000, RoundingMode.HALF_DOWN);

        PI.endComputation(ans);
        this.latch.countDown();
    }
}
