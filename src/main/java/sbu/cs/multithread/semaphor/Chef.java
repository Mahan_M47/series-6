package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    private final Semaphore semaphore;
    public Chef(String name, Semaphore semaphore)
    {
        super(name);
        this.semaphore = semaphore;
    }

    @Override
    public void run()
    {
        semaphore.acquireUninterruptibly();
        System.out.println(super.getName() + " has acquired permit.");

        for (int i = 0; i < 10; i++) {
            Source.getSource();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        semaphore.release();
        System.out.println(super.getName() + " has released permit.");
    }
}
